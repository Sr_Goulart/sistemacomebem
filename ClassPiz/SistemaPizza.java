
package ClassPiz;

import java.util.ArrayList;
import java.util.Scanner;

public class SistemaPizza {
    Scanner leitor = new Scanner (System.in);
    
     ArrayList<Pizzaria> listaPizzaria = new ArrayList<Pizzaria>();
     
     public void cadastraPizzaria(){
          Pizzaria novaPizzaria = new Pizzaria();
         System.out.println("Digite o CNPJ para cadastrar uma Pizzaria\n");
         novaPizzaria.cnpj = leitor.nextInt();
         System.out.println("Digite o nome da Pizzaria\n");
         novaPizzaria.nome = leitor.next();
         System.out.println("Digite o endereço da Pizzaria\n");
         novaPizzaria.endereco = leitor.next();
        listaPizzaria.add(novaPizzaria);
         System.out.println("Pizzaria Cadastrada\n");      
     }
    
     public void cadastraCliente(){
         
         listaPizzaria.forEach((pizzaria) -> {
             pizzaria.pedido.forEach((pedido) -> {
                    Cliente cliente = new Cliente();         
                    System.out.println("Digite o nome do cliente:\n");
                    cliente.nome = leitor.next();
                    System.out.println("Digite o telefone");
                    cliente.telefone = leitor.nextInt();
                    pedido.cliente.add(cliente);
             });
         });     
     }
     
     public void cadastraPizza(){
        listaPizzaria.forEach((pizzaria) -> {
            pizzaria.pedido.forEach((pedido) -> {
                TipoPizza tipoPizza = new TipoPizza();
                
                System.err.println("Digite o nome da Pizza\n");
                tipoPizza.nome = leitor.next();
                System.err.println("Digite a marca da Pizza\n");
                tipoPizza.marca = leitor.next();
                System.err.println("Digite o valor da pizza\n");
                tipoPizza.valor = leitor.nextInt();
                System.err.println("Digite o tamanho da Pizza");
                tipoPizza.tamanho = leitor.next();
                pedido.tipoPizza.add(tipoPizza);
                
                System.err.println("Pizza cadastrada\n"); 
            });
        });   
     }
     public void cadastraPedido(){
         System.out.println("Digite o CNPJ da pizza:\n");
         int novoCnpj = leitor.nextInt();
         
        listaPizzaria.forEach((conferePizzaria) -> {
            if (novoCnpj == conferePizzaria.cnpj) {
              conferePizzaria.pedido.forEach((pedido) -> {
                   System.err.println("Digite o cod do pedido");
                    pedido.cod = leitor.nextInt();
                  pedido.tipoPizza.forEach((tipos) -> {
                            
                            System.err.println("Digite o nome da pizza\n");
                            tipos.nome = leitor.next();
                            System.err.println("Digite o tamanho da pizza\n");
                            tipos.tamanho = leitor.next();
                            
                            
                                    pedido.pedidos.forEach((pedir) -> {
                                       
                                        System.err.println("Digite a quantidade desejada:\n");
                                        pedir.qtdPizza = leitor.nextInt();
                                        
                                        pedir.valorTotal = pedir.qtdPizza * tipos.valor;
                                        
                                        System.err.println("Valor total: " + pedir.valorTotal);
                                    });
                    });
              });
                 
            }else System.out.println("Cnpj invalido");
         });  
     }
     
     public void cadastraEntregador(){
         System.out.println("Digite o CNPJ da Pizzaria");
         int novoCnpj = leitor.nextInt();
         
         listaPizzaria.forEach((pizzaria) -> {
             if (novoCnpj == pizzaria.cnpj) {
                 pizzaria.pedido.forEach((pedir) -> {
                     pedir.entrega.forEach((entregar) -> {
                         Entregador entregador = new Entregador();
                 
                        System.err.println("Digite o nome do entregador:\n");
                        entregador.nome = leitor.next();
                        System.err.println("Digite o endereço do entregador:\n");
                        entregador.endereco = leitor.next();
                        entregar.entregador.add(entregador);
                     });
                 });
                 
             }
         });
     }
     
     public void pedidoEntrega(){
         listaPizzaria.forEach((pizzaria) -> {
             pizzaria.pedido.forEach((pedir) -> {
                 System.err.println("Digite o cod do pedido");
                 int novoPedido = leitor.nextInt();
                 if (novoPedido == pedir.cod) {
                     pedir.entrega.forEach((entregar) -> {
                         System.err.println("Digite o nome do entregador:\n");
                         String novoEntregador = leitor.next();
                         System.err.println("Digite a hora da saida da entrega");
                         entregar.dataHoraSaida = leitor.next();
                         System.err.println("Digite a hora da  da entrega");
                         entregar.dataHoraEntrega = leitor.next();
                         entregar.entregador.forEach((entregas) -> {
                             if (novoEntregador == entregas.nome) {
                                 System.err.println("Digite o endereço de entrega\n");
                                 entregas.endereco = leitor.next();
                                 System.err.println("Digite o nome da entrega\n");
                                 entregas.nome = leitor.next();
                             }
                         });
                        
                     });
                 }else System.out.println("Pedido invalido\n");
 
                
             });
         });
     }
     
     public void RelatorioPagar(){
          System.out.println("Digite o CNPJ da pizza:\n");
         int novoCnpj = leitor.nextInt();
         
        listaPizzaria.forEach((conferePizzaria) -> {
            if (novoCnpj == conferePizzaria.cnpj) {
              conferePizzaria.pedido.forEach((pedido) -> {
                   System.err.println("Digite o cod do pedido");
                    pedido.cod = leitor.nextInt();
                  pedido.tipoPizza.forEach((tipos) -> {
                           
                                    pedido.pedidos.forEach((pedir) -> {
                                       
                                        System.out.println();
                                    });
                    });
              });
                 
            }else System.out.println("Cnpj invalido");
         });  
     }
}
