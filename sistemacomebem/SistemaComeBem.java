
package sistemacomebem;

import ClassPiz.SistemaPizza;
import java.util.Scanner;

public class SistemaComeBem {

    
    public static void main(String[] args) {
         Scanner leitor = new Scanner (System.in);
        SistemaPizza pizzaria  = new SistemaPizza();
          int op;
        do {   
            System.out.println("**********MENU*********");
            System.out.println("1-Cadastrar Pizzaria\n");
            System.out.println("2-Cadastrar Cliente\n");
            System.out.println("3-Cadastrar Pizza P, M, G, GG\n");
            System.out.println("4-Cadastrar Pedido\n");
            System.out.println("5-Cadastra Entregador\n");
            System.out.println("6-Pedido de entrega\n");
            System.out.println("7-Relatorio Conta a pagar\n ");
            System.out.println("8-Sair\n");
            System.out.println("************************");
            op = leitor.nextInt();
            
            if (op < 1 || op >10) {
                System.out.println("Opção invalida\n");
            }
           
            if (op == 1) {
                pizzaria.cadastraPizzaria();
                
                 System.out.println("\n");
            }
           
            if (op == 2) {
               pizzaria.cadastraCliente();
                System.out.println("\n");
            }
            
            if (op ==3) {
              pizzaria.cadastraPizza();
                System.out.println("\n");
            }
            
            if (op == 4) {
               pizzaria.cadastraPedido();
                System.out.println("\n");
            }
            //5-Cadastra Entregador
            if (op == 5) {
               pizzaria.cadastraEntregador();
                System.out.println("\n");
            }
          //6-Pedido de entrega
            if (op == 6) {
            pizzaria.pedidoEntrega();
             System.out.println("\n");
            }
            if (op == 7) {
               pizzaria.RelatorioPagar();
            }
           
        } while (op != 8);
      
 
    }
    
}
